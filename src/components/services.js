import React from 'react'
import './styles/services.css'

import i1 from '../images/ilustraciones/8.svg'
import i2 from '../images/ilustraciones/10.svg'
import i3 from '../images/ilustraciones/4.svg'
import i4 from '../images/ilustraciones/2.svg'
import i5 from '../images/ilustraciones/1.svg'
import i6 from '../images/ilustraciones/6.svg'

export default class Services extends React.Component {
    render() {
        return (
            
            <div className="containerServices contenedor" id="servicios">
              
                <h2> Nuestros Servicios </h2>

                <div className="list-services">
                    <div className="service _s1">

                        <div className="imageS">
                            <img src={i1} />

                        </div>

                        <h3> Planeación </h3>

                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
                    </p>

                    </div>
                    <div className="service _s2">
                        <div className="imageS">
                            <img src={i2} />

                        </div>
                        <h3> Desarollo </h3>

                        <p> Cconsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.
                    </p>
                    </div>
                    <div className="service _s3">
                        <div className="imageS">
                            <img src={i3} />

                        </div>
                        <h3> móvil </h3>

                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                         
                        </div>

                        <div className="service _s4">
                            <div className="imageS">
                                <img src={i4} />

                            </div>
                            <h3> analisis </h3>

                            <p> Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
                         </p>
                         </div>
                        <div className="service _s5">
                            <div className="imageS">
                                <img src={i5} />

                            </div>
                            <h3> atención </h3>

                            <p> do eiusmod tempor incididunt ut Ut enim ad minim veniam, quis nos.
                    </p>
                        </div>
                        <div className="service _s6">
                            <div className="imageS">
                                <img src={i6} />

                            </div>
                            <h3> tecnologias </h3>

                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing.
                    </p>
                        </div>


                    </div>

                </div>
        )
    }
}