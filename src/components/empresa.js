import React from 'react'

import logoEmpresa from '../images/ilustraciones/team.svg'
import './styles/empresa.css'
export default class Empresa extends React.Component {
    render() {
        return (
            <div className="container-precios contenedor" id="empresa">
               
                <h2>nuestra empresa</h2>

                <div className="grid-precios">
                    <div className="item-precio _z2">
                        <img src={logoEmpresa} alt="logito" />
                    </div>
                    <div className="item-precio">
                        <h4> <span>Diseno</span> & <span className="span2">Experiencia</span> en el logo de tu marca </h4>
                        <p className="color1">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                        <button className="btn-saber">saber mas</button>
                    </div>
                </div>

            </div>

        )
    }
}